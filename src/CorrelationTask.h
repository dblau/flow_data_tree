//
// Created by Lukas Kreis on 13.11.17.
//

#ifndef CORRELATION_TASK_H
#define CORRELATION_TASK_H

#include "TChain.h"
#include "TTreeReader.h"

#include <CorrelationManager.h>

class CorrelationTask {
 public:
  CorrelationTask() = default;
  CorrelationTask(std::string filelist, std::string treename);

  void Configure(Qn::CorrelationManager &manager);
  void Run();

  void ReadConfig();

 private:
  std::unique_ptr<TTree> inputTree_;

  /**
   * Make TChain from file list
   * @param filename name of file containing paths to root files containing the input trees
   * @return Pointer to the TChain
   */
  static std::unique_ptr<TChain> MakeChainFromList(const std::string& filename, const std::string& treename);
  bool isSimulation{false};

  Qn::Sampler::Method samplingMode{Qn::Sampler::Method::NONE};
  int nSamples{1};
};

#endif //CORRELATION_TASK_H
