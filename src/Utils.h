//
// Created by eugene on 1/22/19.
//

#ifndef DATATREEFLOW_UTILS_H
#define DATATREEFLOW_UTILS_H

#include "GlobalConfig.h"
#include <DetectorConfiguration.h>

namespace Utils {

std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(const PSDSubevent &subevent,
                                                                          int correctionSteps);

std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(const PSDSubevent &subevent,
                                                                          const std::array<bool,3> &correctionSteps);
/**
 * Factory function for psd detector configuration
 * @param subevent
 * @param applyRecenter
 * @param applyTwist
 * @param applyRescale
 * @return
 */
std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(
    const PSDSubevent &subevent,
    bool applyRecenter = true,
    bool applyTwist = false,
    bool applyRescale = false);

}
#endif //DATATREEFLOW_UTILS_H
