#include <iostream>
#include <iomanip>
#include <chrono>
#include <string>

#include "CorrelationTask.h"
#include "GlobalConfig.h"

int main(int argc, char **argv) {
  using namespace std;

  string filelist{argv[1]};
  string ini_file{argc > 2 ? argv[2] : "flow.ini"};


  if (gConfig->LoadIni(ini_file.c_str())) {
    gConfig->LoadResources();

    CorrelationTask st(filelist, "tree");
    st.ReadConfig();
    std::cout << "go" << std::endl;
    auto start = std::chrono::system_clock::now();
    st.Run();
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << " s\n";
    return 0;
  } else {
    Error(__func__, "Invalid configuration");
    return 1;
  }

  return 1;
}
