#include <memory>

#include <utility>

//
// Created by Lukas Kreis on 29.06.17.
//
#include "CorrectionTask.h"

#include <memory>
#include <iostream>
#include <CorrectionDetector.h>

#include "Utils.h"

#include "Cuts.h"

static const std::vector<std::string> track_names = {
    "proton_pT",
    "mc_proton_pT",
    "proton_y",
    "mc_proton_y",

    "pion_pT",
    "mc_pion_pT",
    "pion_y",
    "mc_pion_y",

    "pion_neg_pT",
    "mc_pion_neg_pT",
    "pion_neg_y",
    "mc_pion_neg_y",

    "track_r1",
    "track_r2"
};

namespace Qn {

CorrectionTask::CorrectionTask(const std::string &filelist, const std::string& incalib, const std::string &treename) :
    out_file_(new TFile("output.root", "RECREATE")),
    in_calibration_file_(new TFile(incalib.c_str(), "READ")),
    out_calibration_file_(new TFile("qn.root", "RECREATE")),
    in_tree_(Qn::CorrectionTask::MakeChain(filelist, treename)),
    out_tree_(nullptr),
    tree_reader_(in_tree_.get()),
    event_(tree_reader_, "DTEvent"),
    manager_()
    {
  out_file_->cd();
  out_tree_ = new TTree("tree", "tree");
}

void CorrectionTask::Run() {
  Initialize();
  QnCorrectionsSetTracingLevel(kWarning);
  std::cout << "Processing..." << std::endl;
  while (tree_reader_.Next()) {
    Process();
  }
  Finalize();
}

void CorrectionTask::Initialize() {
  using namespace flow::cuts;
  using namespace Utils;

  manager_.SetTree(out_tree_);
  gVarManager->InitializeCorrectionManager(manager_);


  using Axes = std::vector<Qn::Axis>;

  Axis axEta("Eta", 20, 1.4, 5.0);
  Axis axY("Rapidity", 15, -0.6, 2.4);
  Axis axPt("Pt", {0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f, 1.2f, 1.4f, 1.8f, 2.2f, 2.6f, 3.0f});
  Axis axMinv("Minv", 20, 1.115683-0.015,1.115683+0.015);

  Axes axesTPC = {axPt, axEta};

  auto confTracks = [](DetectorConfiguration *config) {
    config->SetNormalization(QVector::Normalization::M);
    auto recenter = new Recentering();
    config->AddCorrectionOnQnVector(recenter);
    auto rescale = new TwistAndRescale();
    rescale->SetApplyTwist(true);
    rescale->SetApplyRescale(true);
    rescale->SetTwistAndRescaleMethod(TwistAndRescale::TWRESCALE_doubleHarmonic);
    config->AddCorrectionOnQnVector(rescale);
  };

  auto confMCTracks = [](DetectorConfiguration *config) {
    config->SetNormalization(QVector::Normalization::M);
  };

  auto confMC = [](DetectorConfiguration *config) {
    config->SetNormalization(QVector::Normalization::M);
    bool channels[] = {kTRUE};
    int channelGroups[] = {0};
    ((DetectorConfigurationChannels *) config)->SetChannelsScheme(channels, channelGroups, nullptr);
  };


  /*
   * Defining event variables
   */

  manager_.AddRunEventId("RunNumber", "EventID");
  manager_.AddEventVariable("Centrality");
  manager_.AddCorrectionAxis({"Centrality", {0, 5, 10, 15, 25, 35, 45, 60, 80, 100.}});

  // TODO make configurable
  double yLo = 0.0;
  double yHi = 1.2;

  double ptLoProtons = 0.0;
  double ptHiProtons = 2.0;

  double ptLoMesons = 0.0;
  double ptHiMesons = 2.0;

  /** Protons **/
  {
    const std::string proton_pT = "proton_pT";
    manager_.AddDetector(proton_pT, DetectorType::TRACK, "Phi", "Weight", {axPt}, {1,2,3});
    manager_.AddCut(proton_pT, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(proton_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(proton_pT, {"Pid"}, pid<2212>);
    manager_.SetCorrectionSteps(proton_pT, confTracks);

    const std::string proton_y = "proton_y";
    manager_.AddDetector(proton_y, DetectorType::TRACK, "Phi", "Weight", {axY}, {1,2,3});
    manager_.AddCut(proton_y, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(proton_y, {"Pt"}, range(ptLoProtons, ptHiProtons));
    manager_.AddCut(proton_y, {"Pid"}, pid<2212>);
    manager_.SetCorrectionSteps(proton_y, confTracks);
  }


  /** MC Protons **/
  {
    const std::string mc_proton_pT = "mc_proton_pT";
    manager_.AddDetector(mc_proton_pT, DetectorType::TRACK, "Phi", "Ones", {axPt}, {1, 2, 3});
    manager_.AddCut(mc_proton_pT, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_proton_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(mc_proton_pT, {"Pid"}, pid<2212>);
    manager_.SetCorrectionSteps(mc_proton_pT, confMCTracks);

    const std::string mc_proton_y = "mc_proton_y";
    manager_.AddDetector(mc_proton_y, DetectorType::TRACK, "Phi", "Ones", {axY}, {1, 2, 3});
    manager_.AddCut(mc_proton_y, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_proton_y, {"Pt"}, range(ptLoProtons, ptHiProtons));
    manager_.AddCut(mc_proton_y, {"Pid"}, pid<2212>);
    manager_.SetCorrectionSteps(mc_proton_y, confMCTracks);
  }

  /** Positive pions **/
  {
    const std::string pion_pT = "pion_pT";
    manager_.AddDetector(pion_pT, DetectorType::TRACK, "Phi", "Weight", {axPt}, {1,2,3});
    manager_.AddCut(pion_pT, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(pion_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(pion_pT, {"Pid"}, pid<211>);
    manager_.SetCorrectionSteps(pion_pT, confTracks);

    const std::string pion_y = "pion_y";
    manager_.AddDetector(pion_y, DetectorType::TRACK, "Phi", "Weight", {axY}, {1,2,3});
    manager_.AddCut(pion_y, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(pion_y, {"Pt"}, range(ptLoMesons, ptHiMesons));
    manager_.AddCut(pion_y, {"Pid"}, pid<211>);
    manager_.SetCorrectionSteps(pion_y, confTracks);
  }

  /** MC positive pions **/
  {
    const std::string mc_pion_pT = "mc_pion_pT";
    manager_.AddDetector(mc_pion_pT, DetectorType::TRACK, "Phi", "Ones", {axPt}, {1, 2, 3});
    manager_.AddCut(mc_pion_pT, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_pion_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(mc_pion_pT, {"Pid"}, pid<211>);
    manager_.SetCorrectionSteps(mc_pion_pT, confMCTracks);

    const std::string mc_pion_y = "mc_pion_y";
    manager_.AddDetector(mc_pion_y, DetectorType::TRACK, "Phi", "Ones", {axY}, {1, 2, 3});
    manager_.AddCut(mc_pion_y, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_pion_y, {"Pt"}, range(ptLoMesons, ptHiMesons));
    manager_.AddCut(mc_pion_y, {"Pid"}, pid<211>);
    manager_.SetCorrectionSteps(mc_pion_y, confMCTracks);
  }

  /**  negative pions  **/
  {
    const std::string pion_neg_pT = "pion_neg_pT";
    manager_.AddDetector(pion_neg_pT, DetectorType::TRACK, "Phi", "Weight", {axPt}, {1,2,3});
    manager_.AddCut(pion_neg_pT, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(pion_neg_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(pion_neg_pT, {"Pid"}, pid<-211>);
    manager_.SetCorrectionSteps(pion_neg_pT, confTracks);

    const std::string pion_neg_y = "pion_neg_y";
    manager_.AddDetector(pion_neg_y, DetectorType::TRACK, "Phi", "Weight", {axY}, {1,2,3});
    manager_.AddCut(pion_neg_y, {"TrackType"}, type<ETrackType::kVertexTrack>);
    manager_.AddCut(pion_neg_y, {"Pt"}, range(ptLoMesons, ptHiMesons));
    manager_.AddCut(pion_neg_y, {"Pid"}, pid<-211>);
    manager_.SetCorrectionSteps(pion_neg_y, confTracks);

  }

  /** MC negative pions  **/
  {
    const std::string mc_pion_neg_pT = "mc_pion_neg_pT";
    manager_.AddDetector(mc_pion_neg_pT, DetectorType::TRACK, "Phi", "Ones", {axPt}, {1, 2, 3});
    manager_.AddCut(mc_pion_neg_pT, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_pion_neg_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(mc_pion_neg_pT, {"Pid"}, pid<-211>);
    manager_.SetCorrectionSteps(mc_pion_neg_pT, confMCTracks);

    const std::string mc_pion_neg_y = "mc_pion_neg_y";
    manager_.AddDetector(mc_pion_neg_y, DetectorType::TRACK, "Phi", "Ones", {axY}, {1, 2, 3});
    manager_.AddCut(mc_pion_neg_y, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_pion_neg_y, {"Pt"}, range(ptLoMesons, ptHiMesons));
    manager_.AddCut(mc_pion_neg_y, {"Pid"}, pid<-211>);
    manager_.SetCorrectionSteps(mc_pion_neg_y, confMCTracks);
  }


  /** MC lambda  **/
  {
    const std::string mc_lambda_pT = "mc_lambda_pT";
    manager_.AddDetector(mc_lambda_pT, DetectorType::TRACK, "Phi", "Ones", {axPt}, {1, 2, 3});
    manager_.AddCut(mc_lambda_pT, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_lambda_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(mc_lambda_pT, {"Pid"}, pid<3122>);
    manager_.SetCorrectionSteps(mc_lambda_pT, confMCTracks);

    const std::string mc_lambda_y = "mc_lambda_y";
    manager_.AddDetector(mc_lambda_y, DetectorType::TRACK, "Phi", "Ones", {axY}, {1, 2, 3});
    manager_.AddCut(mc_lambda_y, {"TrackType"}, type<ETrackType::kMCTrack>);
    manager_.AddCut(mc_lambda_y, {"Pt"}, range(ptLoProtons, ptHiProtons));
    manager_.AddCut(mc_lambda_y, {"Pid"}, pid<3122>);
    manager_.SetCorrectionSteps(mc_lambda_y, confMCTracks);
  }


  /** V0 lambda  **/
  {
    const std::string v0_lambda_pT = "v0_lambda_pT";
    manager_.AddDetector(v0_lambda_pT, DetectorType::TRACK, "Phi", "Ones", {axPt,Minv}, {1, 2, 3});
    manager_.AddCut(v0_lambda_pT, {"TrackType"}, type<ETrackType::kV0Track>);
    manager_.AddCut(v0_lambda_pT, {"Rapidity"}, range(yLo, yHi));
    manager_.AddCut(v0_lambda_pT, {"Pid"}, pid<3122>);
    manager_.SetCorrectionSteps(v0_lambda_pT, confTracks);

    const std::string v0_lambda_y = "v0_lambda_y";
    manager_.AddDetector(v0_lambda_y, DetectorType::TRACK, "Phi", "Ones", {axY,Minv}, {1, 2, 3});
    manager_.AddCut(v0_lambda_y, {"TrackType"}, type<ETrackType::kV0Track>);
    manager_.AddCut(v0_lambda_y, {"Pt"}, range(ptLoProtons, ptHiProtons));
    manager_.AddCut(v0_lambda_y, {"Pid"}, pid<3122>);
    manager_.SetCorrectionSteps(v0_lambda_y, confTracks);
  }


  /* ==================== RNDM Sub-Event ================= */
  manager_.AddDetector("track_r1", DetectorType::TRACK, "Phi", "Ones", {},{1,2,3});
  manager_.AddCut("track_r1",
                  {"Pt", "Rapidity", "Pid"},
                  [=](double &pT, double &y, double &pid) {
                    return (pT > ptLoProtons && pT < ptHiProtons) && (y > -0.5 && y < 1.2) && (abs(pid) == 211);
                  });

  manager_.AddDetector("track_r2", DetectorType::TRACK, "Phi", "Ones", {},{1,2,3});
  manager_.AddCut("track_r2",
                  {"Pt", "Rapidity"},
                  [=](double &pT, double &y) { return (pT > ptLoProtons && pT < ptHiProtons) && (y > 0.8 && y < 1.8); });

  manager_.SetCorrectionSteps("track_r1", confTracks);
  manager_.SetCorrectionSteps("track_r2", confTracks);

  /* ==================== PSD Configuration ================= */
  for (const auto &subevent : *gConfig->getPsdSubeventLayout()) {
    auto correctionSteps = gConfig->GetInstance()->getPsdSubeventsCorrectionSteps().at(subevent.getName());

    std::string correctionStepsStr;
    if (correctionSteps > 0) correctionStepsStr.append("RECENTERING");
    if (correctionSteps > 1) correctionStepsStr.append(" + ").append("TWIST");
    if (correctionSteps > 2) correctionStepsStr.append(" + ").append("RESCALE");

    Info(__func__, "%s %s", subevent.getName().c_str(), correctionStepsStr.c_str());
    manager_.AddDetector(subevent.getName(), DetectorType::CHANNEL, "PsdPhi", "PsdEnergy", {}, {1,2});
    manager_.SetCorrectionSteps(subevent.getName(), GeneratePSDConfiguration(subevent, correctionSteps));
  }

  /* ==================== PSI RP for MC ===================== */
  manager_.AddDetector("psi", DetectorType::CHANNEL, "PsiRP", "Ones", {}, {1,2});
  manager_.SetCorrectionSteps("psi", confMC);

  //Initialization of framework
  manager_.Initialize(in_calibration_file_.get());
  AddQAHisto();
  eventAndDetectorQA = manager_.GetEventAndDetectorQAList();
  manager_.SetProcessName("test");
}

void CorrectionTask::Process() {
  manager_.Reset();
  auto event = event_.Get();

  ++nTotalEvents_;
  if (cuts && !cuts->IsGoodEvent(*event)) {
    return;
  }
  ++nGoodEvents_;

  gVarManager->FillEventInfo(*event, manager_.GetVariableContainer());
  manager_.ProcessEvent();
  manager_.FillChannelDetectors();
  int nVertexTracks = event->GetNVertexTracks();
  for (int iTrack = 0; iTrack < nVertexTracks; ++iTrack) {
    auto track = event->GetVertexTrack(iTrack);
    if (cuts && !cuts->IsGoodTrack(*track)) continue;

    gVarManager->FillTrackInfo(*event, *track, manager_.GetVariableContainer());
    manager_.FillTrackingDetectors();
  }

  int nMCTracks = event->GetNMCTracks();
  for (int iTrack = 0; iTrack < nMCTracks; ++iTrack) {
    auto track = event->GetMCTrack(iTrack);

    gVarManager->FillMCTrackInfo(*event, *track, manager_.GetVariableContainer());
    manager_.FillTrackingDetectors();
  }

  int nV0Tracks = event->GetNV0CandidatesMCpid();
  for (int iTrack = 0; iTrack < nV0Tracks; ++iTrack) {
    auto track = event->GetV0CandidateMCpid(iTrack);
//todo    if (cuts && !cuts->IsGoodTrack(*track)) continue;

    gVarManager->FillV0Info(*event, *track, manager_.GetVariableContainer());
    manager_.FillTrackingDetectors();
  }

  manager_.ProcessQnVectors();
}

void CorrectionTask::Finalize() {
  manager_.Finalize();

  out_file_->cd();
  out_tree_->Write();
  out_file_->mkdir("QA")->cd();
  manager_.GetEventAndDetectorQAList()->Write();
  out_file_->Close();

  out_calibration_file_->cd();
  out_calibration_file_->WriteObject(manager_.GetCalibrationList(), "CalibrationHistograms");
  out_calibration_file_->WriteObject(manager_.GetCalibrationQAList(), "CalibrationQAHistograms");

  Info(__func__, "Selected %d out of %d events.", nGoodEvents_, nTotalEvents_);

}

std::unique_ptr<TChain> CorrectionTask::MakeChain(const std::string& filename, const std::string& treename) {
  std::unique_ptr<TChain> chain(new TChain(treename.c_str()));
  std::ifstream in;
  in.open(filename);
  std::string line;
  std::cout << "Adding files to chain:" << std::endl;
  while ((in >> line).good()) {
    if (!line.empty()) {
      chain->AddFile(line.data());
      std::cout << line << std::endl;
    }
  }
  return chain;
}

void CorrectionTask::AddQAHisto() {

  for (const auto &detector : track_names) {
    manager_.AddHisto1D(detector, {"Phi", 500, -4, 4}, "Ones");
    manager_.AddHisto1D(detector, {"Pt", 500, 0, 3}, "Ones");
    manager_.AddHisto1D(detector, {"Rapidity", 500, -1, 3}, "Ones");
    manager_.AddHisto1D(detector, {"Eta", 500, -2, 7}, "Ones");
    manager_.AddHisto1D(detector, {"Pid", 1000, -2500, 2500}, "Ones");
    manager_.AddHisto1D(detector, {"M2", 500, -0.5, 1.2}, "Ones");
    manager_.AddHisto1D(detector, {"P", 500, 0, 10}, "Ones");
    manager_.AddHisto1D(detector, {"Chi2Vtx", 100, 0, 5}, "Ones");

    manager_.AddHisto2D(detector, {{"P", 500, 0, 10}, {"M2", 500, -0.5, 1.2}});
    manager_.AddHisto2D(detector, {{"P", 500, 0, 10}, {"dEdx", 1000, 0., 10.}});
    manager_.AddHisto2D(detector, {{"Phi", 500, -4, 4}, {"Rapidity", 500, -1, 3}});
    manager_.AddHisto2D(detector, {{"Phi", 500, -4, 4}, {"Pt", 500, 0, 3}});
    manager_.AddHisto2D(detector, {{"Phi", 500, -4, 4}, {"Eta", 500, -2, 7}});
    manager_.AddHisto2D(detector, {{"Rapidity", 500, -1, 3}, {"Pt", 500, 0, 3}});
    manager_.AddHisto2D(detector, {{"Eta", 500, -2, 7}, {"Pt", 500, 0, 3}});
  }

  manager_.AddHisto1D("psd1", {"PsdEnergy", 6200, 0, 6200}, "Ones");
  manager_.AddHisto1D("psd1", {"PsdPhi", 500, -4, 4}, "Ones");
  manager_.AddHisto1D("psd2", {"PsdEnergy", 6200, 0, 6200}, "Ones");
  manager_.AddHisto1D("psd2", {"PsdPhi", 500, -4, 4}, "Ones");
  manager_.AddHisto1D("psd3", {"PsdEnergy", 6200, 0, 6200}, "Ones");
  manager_.AddHisto1D("psd3", {"PsdPhi", 500, -4, 4}, "Ones");

  manager_.AddHisto1D("psi", {"PsiRP", 500, -4, 4}, "Ones");

  manager_.AddEventHisto1D({"Centrality", 20, 0, 100});
  manager_.AddEventHisto1D({"Mreco", 700, 0, 700});
  manager_.AddEventHisto1D({"VertexX", 500, -1, 1});
  manager_.AddEventHisto1D({"VertexY", 500, -1, 1});
  manager_.AddEventHisto1D({"VertexZ", 500, -1, 1});

  manager_.AddEventHisto2D({{"VertexX", 500, -1, 1}, {"VertexY", 500, -1, 1}});
  manager_.AddEventHisto2D({{"Centrality", 20, 0, 100}, {"Mreco", 700, 0, 700}});
  manager_.AddEventHisto2D({{"Centrality", 20, 0, 100}, {"PsdEnergyTotal", 6200, 0, 6200}});
}

void CorrectionTask::ReadConfig() {
  using std::string;

  const auto &globalConfig = *gConfig;

  string configFileName = globalConfig.getConfigFileName();
  string configName = globalConfig.getConfigName();
  string cutsConfigName = globalConfig.getCutsConfigName();

  {
    cuts.reset();
    Info(__func__, "Setting cuts config '%s'", globalConfig.getCutsConfig()->GetName());
    this->cuts = std::make_shared<cuts::DataTreeCuts>(globalConfig.getCutsConfig().get());
  }

  psdSubeventLayout = globalConfig.getPsdSubeventLayout();
  if (psdSubeventLayout) {
    for (const auto &subevent : *psdSubeventLayout) {
      std::ostringstream moduleIDStr;

      moduleIDStr << subevent.getName() << " ";
      moduleIDStr << "[ ";
      for (int module : subevent.getModuleIDs()) moduleIDStr << module << ", ";
      moduleIDStr << "]";
      Info(__func__, "%s", moduleIDStr.str().c_str());
    }
  } else {

  }
}

}
