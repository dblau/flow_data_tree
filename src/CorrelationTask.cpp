#include <utility>
#include <TFileCollection.h>

//
// Created by Lukas Kreis on 13.11.17.
//

#include "CorrelationTask.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TTreeReaderValue.h"

#include "GlobalConfig.h"

CorrelationTask::CorrelationTask(std::string filelist, std::string treename) :
    inputTree_(CorrelationTask::MakeChainFromList(std::move(filelist), std::move(treename))) {}

void CorrelationTask::Configure(Qn::CorrelationManager &a) {
  using QVectorPtr = Qn::QVectorPtr;
  using std::vector;
  using std::string;

  auto scalar = [](const std::vector<QVectorPtr> &a) -> double {
    return a[0].x(2) * a[1].x(2) + a[0].y(2) * a[1].y(2);
  };

  auto XY = [](const std::vector<QVectorPtr> &a) {
    return 2 * a[0].x(1) * a[1].y(1);
  };
  auto YX = [](const std::vector<QVectorPtr> &a) {
    return 2 * a[0].y(1) * a[1].x(1);
  };
  auto XX = [](const std::vector<QVectorPtr> &a) {
    return 2 * a[0].x(1) * a[1].x(1);
  };
  auto YY = [](const std::vector<QVectorPtr> &a) {
    return 2 * a[0].y(1) * a[1].y(1);
  };

  auto Y2XY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(2) * a[1].x(1) * a[2].y(1);
  };
  auto Y2YX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(2) * a[1].y(1) * a[2].x(1);
  };
  auto X2XX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(2) * a[1].x(1) * a[2].x(1);
  };
  auto X2YY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(2) * a[1].y(1) * a[2].y(1);
  };

  auto X2XY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(2) * a[1].x(1) * a[2].y(1);
  };
  auto X2YX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(2) * a[1].y(1) * a[2].x(1);
  };
  auto Y2XX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(2) * a[1].x(1) * a[2].x(1);
  };
  auto Y2YY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(2) * a[1].y(1) * a[2].y(1);
  };

  auto Y3YYY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].y(1) * a[3].y(1);
  };
  auto Y3XYY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].y(1) * a[3].y(1);
  };
  auto Y3YXY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].x(1) * a[3].y(1);
  };
  auto Y3YYX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].y(1) * a[3].x(1);
  };
  auto Y3YXX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].y(1) * a[2].x(1) * a[3].x(1);
  };
  auto Y3XYX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].y(1) * a[3].x(1);
  };
  auto Y3XXY = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].x(1) * a[3].y(1);
  };
  auto Y3XXX = [](const std::vector<QVectorPtr> &a) {
    return a[0].y(3) * a[1].x(1) * a[2].x(1) * a[3].x(1);
  };

  auto X3YYY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].y(1) * a[3].y(1);
  };
  auto X3XYY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].y(1) * a[3].y(1);
  };
  auto X3YXY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].x(1) * a[3].y(1);
  };
  auto X3YYX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].y(1) * a[3].x(1);
  };
  auto X3YXX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].y(1) * a[2].x(1) * a[3].x(1);
  };
  auto X3XYX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].y(1) * a[3].x(1);
  };
  auto X3XXY = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].x(1) * a[3].y(1);
  };
  auto X3XXX = [](const std::vector<QVectorPtr> &a) {
    return a[0].x(3) * a[1].x(1) * a[2].x(1) * a[3].x(1);
  };

  a.SetOutputFile("corr.root");

  const std::vector<std::string> detectors{
      "psd1", "psd2", "psd3",

//      "track_r1", "track_r2",

      "proton_pT", "proton_y",

      "pion_pT", "pion_y",

      "pion_neg_pT", "pion_neg_y",
  };


  const std::vector<std::string> detectorsPSD{detectors.begin(), detectors.begin() + 3};
  const std::vector<std::string> detectorsTracks{detectors.begin() + 3, detectors.end()};

  auto stringify = [](const std::vector<std::string> &detectors) {
    const char *del = ", ";
    std::ostringstream detectorsStringStream{};
    std::copy(detectors.begin(), detectors.end(), std::ostream_iterator<std::string>(detectorsStringStream, del));
    std::string result_raw{detectorsStringStream.str()};
    return std::string{result_raw.begin(), result_raw.end() - std::strlen(del)};
  };

  Info(__func__, "Detectors: [%s]", stringify(detectors).c_str());
  Info(__func__, "Track Detectors: [%s]", stringify(detectorsTracks).c_str());
  Info(__func__, "PSD Detectors: [%s]", stringify(detectorsPSD).c_str());

  a.AddEventAxis({"Centrality", {0, 5, 10, 15, 25, 35, 45, 60, 80, 100.}});

  const std::string
      detectorsMC[] = {"mc_proton_pT", "mc_proton_y", "mc_pion_pT", "mc_pion_y"};

  const vector<vector<string> > vRefVsRefCorrelation{
      {"psd1", "psd2"},
      {"psd2", "psd3"},
      {"psd3", "psd1"},
  };

  const std::string sPsdPsd[] = {"psd1_psd2", "psd2_psd3", "psd3_psd1",};

  const std::string sPsdPsdPsd = "psd1_psd2_psd3";
  const std::string sPsdPsdPsdName = "psd1, psd2, psd3";

  a.SetResampling(samplingMode, nSamples);

  /**
   * Correlations of all detectors vs PsiRP
   */
  if (isSimulation) {
    for (const auto &det : detectors) {
      vector<string> correlationDetectors{det, "psi"};
      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};

      a.AddCorrelation(det + "_psi_XX", correlationDetectors, XX, correlationWeights);
      a.AddCorrelation(det + "_psi_YY", correlationDetectors, YY, correlationWeights);
      a.AddCorrelation(det + "_psi_XY", correlationDetectors, XY, correlationWeights);
      a.AddCorrelation(det + "_psi_YX", correlationDetectors, YX, correlationWeights);
    }

    for (const auto &iDet : detectorsMC) {
      vector<string> correlationDetectors{iDet, "psi"};
      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};
      a.AddCorrelation(iDet + "_psi_XX", correlationDetectors, XX, correlationWeights);
      a.AddCorrelation(iDet + "_psi_YY", correlationDetectors, YY, correlationWeights);
      a.AddCorrelation(iDet + "_psi_XY", correlationDetectors, XY, correlationWeights);
      a.AddCorrelation(iDet + "_psi_YX", correlationDetectors, YX, correlationWeights);
    }
  }

  /**
   * Correlations of tracks vs PSD
   */
  for (const auto &psd : detectorsPSD) {
    for (const auto &track : detectorsTracks) {
      a.AddCorrelation(track + "_" + psd + "_XX", {track, psd}, XX, {Qn::Weight::OBSERVABLE, Qn::Weight::REFERENCE});
      a.AddCorrelation(track + "_" + psd + "_YY", {track, psd}, YY, {Qn::Weight::OBSERVABLE, Qn::Weight::REFERENCE});
      a.AddCorrelation(track + "_" + psd + "_XY", {track, psd}, XY, {Qn::Weight::OBSERVABLE, Qn::Weight::REFERENCE});
      a.AddCorrelation(track + "_" + psd + "_YX", {track, psd}, YX, {Qn::Weight::OBSERVABLE, Qn::Weight::REFERENCE});
    }
  }

  for (ushort iDet = 0; iDet < 3; ++iDet) {
    a.AddCorrelation(sPsdPsd[iDet] + "_XX",
                     vRefVsRefCorrelation[iDet],
                     XX,
                     {Qn::Weight::REFERENCE, Qn::Weight::REFERENCE});
    a.AddCorrelation(sPsdPsd[iDet] + "_YY",
                     vRefVsRefCorrelation[iDet],
                     YY,
                     {Qn::Weight::REFERENCE, Qn::Weight::REFERENCE});
    a.AddCorrelation(sPsdPsd[iDet] + "_XY",
                     vRefVsRefCorrelation[iDet],
                     XY,
                     {Qn::Weight::REFERENCE, Qn::Weight::REFERENCE});
    a.AddCorrelation(sPsdPsd[iDet] + "_YX",
                     vRefVsRefCorrelation[iDet],
                     YX,
                     {Qn::Weight::REFERENCE, Qn::Weight::REFERENCE});
//
    for (const auto &sTrack : detectorsTracks) {
      vector<string> correlationDetectors;
      correlationDetectors.push_back(sTrack);
      correlationDetectors.insert(correlationDetectors.end(),
                                  vRefVsRefCorrelation[iDet].begin(),
                                  vRefVsRefCorrelation[iDet].end());

      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};

      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_X2XX",
                       correlationDetectors,
                       X2XX, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_X2YY",
                       correlationDetectors,
                       X2YY, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_Y2XY",
                       correlationDetectors,
                       Y2XY, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_Y2YX",
                       correlationDetectors,
                       Y2YX, correlationWeights);

      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_Y2XX",
                       correlationDetectors,
                       Y2XX, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_Y2YY",
                       correlationDetectors,
                       Y2YY, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_X2XY",
                       correlationDetectors,
                       X2XY, correlationWeights);
      a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_X2YX",
                       correlationDetectors,
                       X2YX, correlationWeights);

    }
  }

}

void CorrelationTask::Run() {
  Qn::CorrelationManager a(inputTree_.get());
  a.EnableDebug();
  Configure(a);
  std::cout << "CorrelationManager::Run()..." << std::endl;
  a.Run();
  std::cout << "Done." << std::endl;
}

std::unique_ptr<TChain> CorrelationTask::MakeChainFromList(const std::string& filename, const std::string& treename) {
  std::unique_ptr<TChain> chain(new TChain(treename.data()));

  TFileCollection fc("fc", "", filename.c_str());
  chain->AddFileInfoList(reinterpret_cast<TCollection *>(fc.GetList()));
  chain->ls();
  return chain;
}

void CorrelationTask::ReadConfig() {

  const auto &globalConfig = *gConfig;

  isSimulation = globalConfig.IsSimulation();
  Info(__func__, "%s", isSimulation ? "MC mode" : "Reco mode");

  if (globalConfig.getSamplingMode() != "NONE") {
    if (globalConfig.getSamplingMode() == "SUBSAMPLING") {
      samplingMode = Qn::Sampler::Method::SUBSAMPLING;
    } else if (globalConfig.getSamplingMode() == "BOOTSTRAP") {
      samplingMode = Qn::Sampler::Method::BOOTSTRAP;
    } else {
      throw std::logic_error(globalConfig.getSamplingMode() + " is not known");
    }

    nSamples = globalConfig.getNSamples();
    Info(__func__, "Sampling Mode %s(%d)", globalConfig.getSamplingMode().c_str(), nSamples);
  }
}
