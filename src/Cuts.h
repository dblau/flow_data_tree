//
// Created by eugene on 9/9/19.
//

#ifndef DATATREEFLOW_CUTS_H
#define DATATREEFLOW_CUTS_H

#include <functional>
#include <TMath.h>

enum class ETrackType : int {
  kVertexTrack = 0,
  kMCTrack,
  kV0Track
};

namespace flow {

namespace cuts {

template<ETrackType trackType>
bool type(double tt) {
  return static_cast<int>(trackType) == TMath::Nint(tt);
}

inline std::function<bool(double)> range(double lo, double hi) {
  return [=](double x) { return lo <= x && x <= hi; };
}

inline std::function<bool(double)> rangeStrict(double lo, double hi) {
  return [=](double x) { return lo < x && x < hi; };
}

template<int p>
bool pid(double _pid) {
  return TMath::Nint(_pid) == p;
}

inline std::function<bool(double)> pid(int pid) {
  return [=](double _pid) { return TMath::Nint(_pid) == pid; };
}

}

}

#endif //DATATREEFLOW_CUTS_H
