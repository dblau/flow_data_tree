//
// Created by eugene on 1/22/19.
//
#ifndef DATATREEFLOW_GLOBALCONFIG_H
#define DATATREEFLOW_GLOBALCONFIG_H

#include <fstream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include <TFile.h>

#include <cuts/DataTreeCutsConfig.h>
#include <qa/DataTreeQAConfig.h>
#include <centrality/Getter.h>
#include <pid/Getter.h>

class PSDSubevent {

 public:

  PSDSubevent(const std::string &name, const std::vector<int> &moduleIDs) : name(name), moduleIDs(moduleIDs) {}
  PSDSubevent(const std::string &name, int idLo, int idHi) {
    std::vector<int> moduleIDs;
    for (int id = idLo; id <= idHi; ++id) { moduleIDs.push_back(id); }
    this->name = name;
    this->moduleIDs = moduleIDs;
  }

  const std::string &getName() const {
    return name;
  }
  const std::vector<int> &getModuleIDs() const {
    return moduleIDs;
  }

 private:
  std::string name{""};
  std::vector<int> moduleIDs;
};

using PSDSubeventLayout = std::vector<PSDSubevent>;

#define gConfig GlobalConfig::GetInstance()

class GlobalConfig {

 public:
  enum class EDieStrategy {
    kReturnFalse,
    kThrow
  };

  EDieStrategy DIE_STRATEGY{EDieStrategy::kThrow};

  static GlobalConfig *GetInstance() {
    static GlobalConfig *INSTANCE = nullptr;

    if (!INSTANCE) {
      INSTANCE = new GlobalConfig;
    }

    return INSTANCE;
  }

  int getNPsdModules() const {
    return nPsdModules;
  }

  std::shared_ptr<PSDSubeventLayout> getPsdSubeventLayout() const {
    auto iter = psdLayouts.find(psdSubeventLayoutName);

    if (iter == psdLayouts.end()) {
      return std::shared_ptr<PSDSubeventLayout>();
    }

    return iter->second;
  }

  std::string getConfigFileName() const {
    return configFileName;
  }
  std::string getConfigName() const {
    return configName;
  }
  std::string getCutsConfigName() const {
    return cutsConfigName;
  }
  const std::shared_ptr<DataTreeQAConfig> &getQAConfig() const {
    return qaConfig;
  }
  const std::shared_ptr<cuts::DataTreeCutsConfig> &getCutsConfig() const {
    return cutsConfig;
  }
  std::string getCentralityFileName() const {
    return centralityFileName;
  }
  std::string getCentralityVariable() const {
    return centralityVariable;
  }
  const std::shared_ptr<Centrality::Getter> &getCentralityGetter() const {
    return centralityGetter;
  }
  std::string getPidFileName() const {
    return pidFileName;
  }
  std::string getPidVariable() const {
    return pidVariable;
  }
  double getPidPurity() const {
    return pidPurity;
  }
  const std::shared_ptr<Pid::BaseGetter> &getPidGetter() const {
    return pidGetter;
  }
  bool IsSimulation() const {
    return isSimulation;
  }
  const std::string &getSamplingMode() const {
    return samplingMode;
  }
  int getNSamples() const {
    return nSamples;
  }
  const std::map<std::string, int> &getPsdSubeventsCorrectionSteps() const {
    return psdSubeventsCorrectionSteps;
  }

  void Print() const {

  }

  bool LoadIni(const char *filename = "flow.ini");
  bool LoadResources();

 protected:
  void DieWith(const std::string &message) {
    if (DIE_STRATEGY == EDieStrategy::kReturnFalse) {
      Error("GlobalConfig", "%s", message.c_str());
    } else {
      throw std::logic_error(message);
    }
  }

  template<class T>
  bool LoadResource(const std::string &src, const std::string &name, std::shared_ptr<T> &holder) {
    Info(__func__, "Loading '%s' from '%s'", name.c_str(), src.c_str());

    holder.reset();
    std::unique_ptr<TFile> ff(TFile::Open(src.c_str(), "read"));

    if (ff.get() == nullptr) {
      DieWith("source " + src + " is not available");
      return false;
    }

    auto obj = dynamic_cast<T *>(ff->Get(name.c_str()));

    if (!obj) {
      DieWith("object " + name + " is not available in " + src);
      return false;
    }

    holder = std::shared_ptr<T>(obj);
    return true;
  }

  bool isInitialized{false};

  std::map<std::string, std::shared_ptr<PSDSubeventLayout >> psdLayouts;

  // options initialized with Load* functions
  bool isSimulation{false};

  int nPsdModules{-1};
  std::string psdSubeventLayoutName{""};

  // cuts configuration
  std::string configFileName{""};
  std::string configName{""};
  std::string cutsConfigName{""};

  std::shared_ptr<DataTreeQAConfig> qaConfig;
  std::shared_ptr<cuts::DataTreeCutsConfig> cutsConfig;

  // centrality
  std::string centralityFileName{""};
  std::string centralityVariable{""};

  std::shared_ptr<Centrality::Getter> centralityGetter;

  // pid
  std::string pidFileName{""};
  std::string pidVariable{""};
  double pidPurity{-1};

  std::shared_ptr<Pid::BaseGetter> pidGetter;

  // sampling
  std::string samplingMode{""};
  int nSamples{1};

  // psd subevent-specific options
  const int PSD_CORRECTION_STEPS_DEFAULT = 1;
  std::map<std::string, int> psdSubeventsCorrectionSteps;

 private:
  GlobalConfig() {
    _initPsdLayouts();
  }

  void _initPsdLayouts() {
    psdLayouts.emplace("na61_44", std::shared_ptr<PSDSubeventLayout>(
        new PSDSubeventLayout({
                                  PSDSubevent("psd1",
                                              {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}),
                                  PSDSubevent("psd2",
                                              {17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28}),
                                  PSDSubevent("psd3",
                                              {29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44})
                              })));

    psdLayouts.emplace("cbm44", std::shared_ptr<PSDSubeventLayout>(
        new PSDSubeventLayout({
                                  PSDSubevent("psd1",
                                              {19, 20, 25, 26}),
                                  PSDSubevent("psd2",
                                              {12, 13, 14, 15, 18, 21, 24, 27, 30, 31, 32, 33}),
                                  PSDSubevent("psd3",
                                              {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 22, 23, 28, 29, 34, 35, 36,
                                               37, 38, 39, 40, 41, 42, 43, 44})
                              })));

    psdLayouts.emplace("cbm46", std::shared_ptr<PSDSubeventLayout>(
        new PSDSubeventLayout({
                                  PSDSubevent("psd1",  1,  8),
                                  PSDSubevent("psd2",  9, 24),
                                  PSDSubevent("psd3", 25, 46)
                              })));

    psdLayouts.emplace("cbm46_2", std::shared_ptr<PSDSubeventLayout>(
        new PSDSubeventLayout({
                                  PSDSubevent("psd1",  1,  8),
                                  PSDSubevent("psd2",  9, 34),
                                  PSDSubevent("psd3", 35, 46)
                              })));
  }
};

#endif //DATATREEFLOW_GLOBALCONFIG_H
