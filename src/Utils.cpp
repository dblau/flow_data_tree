//
// Created by eugene on 1/22/19.
//

#include "Utils.h"
#include "DataTreeVarManager.h"

namespace Utils {

std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(const PSDSubevent &subevent,
                                                                          const int correctionSteps) {
  if (correctionSteps == 1) {
    return GeneratePSDConfiguration(subevent, true, false, false);
  } else if (correctionSteps == 2) {
    return GeneratePSDConfiguration(subevent, true, true, false);
  } else if (correctionSteps == 3) {
    return GeneratePSDConfiguration(subevent, true, true, true);
  }

  throw std::logic_error(std::to_string(correctionSteps) + " is not a valid correction steps definition");
}

std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(const PSDSubevent &subevent,
                                                                          const std::array<bool,3> &correctionSteps) {
  return GeneratePSDConfiguration(subevent, correctionSteps[0], correctionSteps[1], correctionSteps[2]);
}

std::function<void(Qn::DetectorConfiguration *)> GeneratePSDConfiguration(const PSDSubevent &subevent,
                                                                          const bool applyRecenter,
                                                                          const bool applyTwist,
                                                                          const bool applyRescale) {
  using namespace Qn;

  return [=](DetectorConfiguration *config) {
    config->SetNormalization(QVector::Normalization::M);
    if (applyRecenter) {
      auto recenter = new Recentering();
      recenter->SetApplyWidthEqualization(false);
      config->AddCorrectionOnQnVector(recenter);

      if (applyTwist || applyRescale) {
        auto twistAndRescale = new TwistAndRescale;
        twistAndRescale->SetApplyTwist(applyTwist);
        twistAndRescale->SetApplyRescale(applyRescale);
        twistAndRescale->SetTwistAndRescaleMethod(TwistAndRescale::TWRESCALE_doubleHarmonic);
        config->AddCorrectionOnQnVector(twistAndRescale);
      }
    }

    bool *channels = new bool[gConfig->getNPsdModules()];
    for (int iChannel = 0; iChannel < gConfig->getNPsdModules(); ++iChannel) channels[iChannel] = false;
    for (int moduleID : subevent.getModuleIDs()) channels[moduleID - 1] = true;

    int channelGroups[gConfig->getNPsdModules()];
    for (auto &cg : channelGroups) cg = 0;

    ((DetectorConfigurationChannels *) config)->SetChannelsScheme(channels, channelGroups, nullptr);
  };

}

}

