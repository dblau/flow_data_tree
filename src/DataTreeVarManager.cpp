//
// Created by eugene on 1/18/19.
//

#include <TMath.h>
#include <TFile.h>

#include "DataTreeVarManager.h"
#include "Cuts.h"

void DataTreeVarManager::FillEventInfo(const DataTreeEvent &evt, double *values) {

  values[kEventID] = evt.GetEventId();
  values[kRunNumber] = evt.GetRunId();
  values[kPsiRP] = evt.GetRPAngle();

  values[kVertexX] = evt.GetVertexPositionComponent(0);
  values[kVertexY] = evt.GetVertexPositionComponent(1);
  values[kVertexZ] = evt.GetVertexPositionComponent(2);
  values[kPsdEnergyTotal] = evt.GetPSDEnergy();

  int nGoodVertexTracks = 0;
  for (int iVertexTrack = 0; iVertexTrack < evt.GetNVertexTracks(); ++iVertexTrack) {
    if (this->cuts->IsGoodTrack(*evt.GetVertexTrack(iVertexTrack))) nGoodVertexTracks++;
  }
  values[kMreco] = nGoodVertexTracks;

  DataTreePSDModule *psd = nullptr;

  for (int ich = 0; ich < nPsdModules; ++ich) {

    psd = evt.GetPSDModule(ich);

    if (psd == nullptr) {
      Warning("FillEventInfo", "NO PSD MODULE #%d IN DATATREE", ich);
      continue;
    }

    const double x = psd->GetPositionComponent(0);
    const double y = psd->GetPositionComponent(1);
    const double phi = TMath::ATan2(y, x);
    const double weight = psd->GetEnergy();

    values[kPsdEnergy + ich] = weight;
    values[kPsdPhi + ich] = phi;
  }

  if (centralityGetter != nullptr && kCentralityVariable >= 0) {
    values[kCentrality] = centralityGetter->GetCentrality(static_cast<float>(values[kCentralityVariable]));
  }
}

void DataTreeVarManager::FillTrackInfo(const DataTreeEvent &evt, const DataTreeTrack &track, double *values) {
  const double p = track.GetP();
  const double dEdx = track.GetdEdx(EnumTPC::kTPCAll);
  const int q = track.GetCharge();
  values[kWeight] = 1.;

  values[kP] = p;
  values[kQP] = q * p;
  values[kdEdx] = dEdx;
  values[kPt] = track.GetPt();
  values[kPhi] = track.GetPhi();
  values[kCharge] = track.GetCharge();
  values[kEta] = track.GetEta();
  values[kChi2Vtx] = track.GetVtxChi2();


  double y = -999;

  double m2 = -999.;
  auto tofHitID = track.GetTOFHitId();
  if (tofHitID >= 0 && evt.GetNTOFHits() > tofHitID) {
    auto tofHit = evt.GetTOFHit(tofHitID);
    m2 = tofHit->GetSquaredMass();
  }

  values[kM2] = m2;

  int pid = -1;
  if (pidGetter && kPidVariable >= 0) {
    pid = pidGetter->GetPid(values[kQP], values[kPidVariable], pidPurity);
  }


  if (pid == 2212 || pid == -2212) {
    double pz = track.GetPz();
    double e = sqrt(p * p + 0.938 * 0.938);
    y = 0.5 * log((e + pz) / (e - pz)) - yBeam;

  } else if (pid == 321 || pid == -321) {
    double pz = track.GetPz();
    double e = sqrt(p * p + 0.5 * 0.5);
    y = 0.5 * log((e + pz) / (e - pz)) - yBeam;

  } else {
    y = track.GetRapidity() - yBeam;
    if ((pid == 211 || pid == -211)) {
//      const float eff = heff[2].GetBinContent(heff[2].FindBin(y, track.GetPt()));
//      values[kWeight] = eff > 0.05 ? 1 / eff : 1.;
    }
  }

  values[kPid] = pid;
  values[kRapidity] = y;
  values[kTrackType] = static_cast<double>(ETrackType::kVertexTrack);

}

void DataTreeVarManager::FillMCTrackInfo(const DataTreeEvent &event, const DataTreeMCTrack &track, double *values) {
  values[kPt] = track.GetPt();
  values[kP] = track.GetP();
  values[kRapidity] = track.GetRapidity() - yBeam;
  values[kPhi] = track.GetPhi();
  values[kPid] = track.GetPdgId();
  values[kCharge] = track.GetCharge();
  values[kEta] = track.GetEta();
  values[kTrackType] = static_cast<double>(ETrackType::kMCTrack);
}

void DataTreeVarManager::FillV0Info(const DataTreeEvent &evt, const DataTreeV0Candidate &track, double *values) {
  const double p = track.GetP();
  const double e = track.GetEnergy();

  values[kP] = p;
  values[kPt] = track.GetPt();
  values[kPhi] = track.GetPhi();
  values[kCharge] = track.GetCharge();
  values[kPid] = track.GetPdgId();
  values[kEta] = track.GetEta();
  values[kMinv] = TMath::Sqrt(e*e-p*p);;
  values[kRapidity] = track.GetRapidity() - yBeam;
  values[kTrackType] = static_cast<double>(ETrackType::kV0Track);

}

void DataTreeVarManager::InitializeVarMap(int nPsdModules) {
  varMap.clear();
  varSize = 0;

  this->nPsdModules = nPsdModules;
  Info("VarManager", "Initializing VarManager with %d PSD modules", nPsdModules);
  kEventID = AddVariable("EventID");
  kRunNumber = AddVariable("RunNumber");
  kCentrality = AddVariable("Centrality");

  kVertexX = AddVariable("VertexX");
  kVertexY = AddVariable("VertexY");
  kVertexZ = AddVariable("VertexZ");
  kPsdEnergyTotal = AddVariable("PsdEnergyTotal");
  kMreco = AddVariable("Mreco");

  kTrackType = AddVariable("TrackType");

  kM2 = AddVariable("M2");
  kMinv = AddVariable("MInv");
  kP = AddVariable("P");
  kQP = AddVariable("QP");
  kdEdx = AddVariable("dEdx");
  kPid = AddVariable("Pid");
  kPt = AddVariable("Pt");
  kEta = AddVariable("Eta");
  kRapidity = AddVariable("Rapidity");
  kCharge = AddVariable("Charge");
  kPhi = AddVariable("Phi");
  kWeight = AddVariable("Weight"); // 1/eff
  kChi2Vtx = AddVariable("Chi2Vtx");

  kPsdEnergy = AddVariable("PsdEnergy", nPsdModules);
  kPsdPhi = AddVariable("PsdPhi", nPsdModules);
  kPsiRP = AddVariable("PsiRP");

  for (auto var : varMap) {
    printf("%3d -> %20s:%4d\n", std::get<1>(var), std::get<0>(var).c_str(), std::get<2>(var));
  }
}

void DataTreeVarManager::InitializeCorrectionManager(Qn::CorrectionManager &manager) {
  using std::get;
  for (auto var : varMap) {
    manager.AddVariable(get<0>(var), get<1>(var), get<2>(var));
  }
}

int DataTreeVarManager::AddVariable(const std::string& name, int NChannels) {
  varMap.emplace_back(name, varSize, NChannels);
  int varID = varSize;
  varSize += NChannels;
  return varID;
}

void DataTreeVarManager::LoadConfig(const GlobalConfig &globalConfig) {
  using std::string;

  string centralityVariable = gConfig->getCentralityVariable();
  string pidVariable = gConfig->getPidVariable();
  pidPurity = gConfig->getPidPurity();


  int nPsdModules = gConfig->getNPsdModules();
  InitializeVarMap(nPsdModules);

  cuts = std::make_shared<cuts::DataTreeCuts>(gConfig->getCutsConfig().get());
  centralityGetter = gConfig->getCentralityGetter();
  pidGetter = gConfig->getPidGetter();
  yBeam = gConfig->getQAConfig()->GetBeamRapidity();


  if (!centralityVariable.empty()) {
    auto varIter = std::find_if(varMap.begin(), varMap.end(), [centralityVariable](std::tuple<std::string, int, int> var) {
      return centralityVariable == std::get<0>(var);
    });

    if (varIter != varMap.end()) {
      Info("VarManager", "Using '%s' for centrality", std::get<0>(*varIter).c_str());
      kCentralityVariable = std::get<1>(*varIter);
    } else {
      throw std::logic_error(Form("Variable '%s' was not found", std::get<0>(*varIter).c_str()));
    }
  }


  if (!pidVariable.empty()) {
    auto varIter = std::find_if(varMap.begin(), varMap.end(), [pidVariable](std::tuple<std::string, int, int> var) {
      return pidVariable == std::get<0>(var);
    });

    if (varIter != varMap.end()) {
      Info("VarManager", "Using '%s' for pid", std::get<0>(*varIter).c_str());
      kPidVariable = std::get<1>(*varIter);
    } else {
      throw std::logic_error(Form("Variable '%s' was not found", std::get<0>(*varIter).c_str()));
    }
  }
}


