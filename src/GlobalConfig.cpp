//
// Created by eugene on 3/28/19.
//

#include "GlobalConfig.h"

bool GlobalConfig::LoadIni(const char *filename) {
  Info(__func__, "Loading '%s'", filename);
  using std::string;
  namespace po = boost::program_options;

  const bool allow_unregistered = true;

  po::options_description common_opts;
  common_opts.add_options()
      ("nPsdModules", po::value<int>()->required())
      ("psdLayoutName", po::value<string>()->required())
      ("isSimulation", po::value<bool>()->default_value(false));

  po::options_description cuts_opts;
  cuts_opts.add_options()
      ("cuts.configFile", po::value<string>()->required())
      ("cuts.configName", po::value<string>()->required())
      ("cuts.cutsConfig", po::value<string>());

  po::options_description centrality_opts;
  centrality_opts.add_options()
      ("centrality.centralityFile", po::value<string>()->required())
      ("centrality.centralityVariable", po::value<string>()->required());

  po::options_description pid_opts;
  pid_opts.add_options()
      ("pid.pidFile", po::value<string>()->required())
      ("pid.pidVariable", po::value<string>()->required())
      ("pid.pidPurity", po::value<double>()->required());

  po::options_description sampling_opts;
  sampling_opts.add_options()
      ("sampling.samplingMode", po::value<string>()->default_value("NONE"))
      ("sampling.nSamples", po::value<int>()->default_value(1));

  po::options_description psd_subevents_opts;
  for (const string &det : {"psd1", "psd2", "psd3"}) {
    po::options_description specific_subevent_opts;
    specific_subevent_opts.add_options()
        ((det + "." + "correctionSteps").c_str(), po::value<int>()->default_value(PSD_CORRECTION_STEPS_DEFAULT));

    psd_subevents_opts.add(specific_subevent_opts);
  }

  po::options_description all;
  all
      .add(common_opts)
      .add(cuts_opts)
      .add(centrality_opts)
      .add(pid_opts)
      .add(sampling_opts)
      .add(psd_subevents_opts);

  po::variables_map vm;

  std::ifstream config_fd(filename);
  po::store(po::parse_config_file(config_fd, all, allow_unregistered), vm);

  try {
    po::notify(vm);
  } catch (const po::error &e) {
    Error(__func__, "%s", e.what());
    return false;
  }

  auto GetOptInt = [vm](const string &optName, int defval) {
    if (vm.count(optName)) {
      return vm[optName].as<int>();
    }
    return defval;
  };

  auto GetOptString = [vm](const string &optName, const char *defval) {
    if (vm.count(optName)) {
      return vm[optName].as<string>();
    }

    return string(defval);
  };

  auto GetOptBool = [vm](const string &optName, bool defval) {
    if (vm.count(optName)) {
      return vm[optName].as<bool>();
    }

    return defval;
  };

  auto GetOptDouble = [vm](const string &optName, double defval) {
    if (vm.count(optName)) {
      return vm[optName].as<double>();
    }

    return defval;
  };
  nPsdModules = GetOptInt("nPsdModules", -1);
  psdSubeventLayoutName = GetOptString("psdLayoutName", "");
  isSimulation = GetOptBool("isSimulation", false);

  configFileName = GetOptString("cuts.configFile", "QAConfigurations.root");
  configName = GetOptString("cuts.configName", "");
  cutsConfigName = GetOptString("cuts.cutsConfig", "");

  centralityFileName = GetOptString("centrality.centralityFile", "");
  centralityVariable = GetOptString("centrality.centralityVariable", "");

  pidFileName = GetOptString("pid.pidFile", "");
  pidVariable = GetOptString("pid.pidVariable", "");
  pidPurity = GetOptDouble("pid.pidPurity", 0.9);

  samplingMode = GetOptString("sampling.samplingMode", "NONE");
  nSamples = GetOptInt("sampling.nSamples", 1);

  for (const string &det : {"psd1", "psd2", "psd3"}) {
    auto correctionSteps = GetOptInt(det + "." + "correctionSteps", PSD_CORRECTION_STEPS_DEFAULT);
    psdSubeventsCorrectionSteps.emplace(det, correctionSteps);
  }

  isInitialized = true;
  return true;
}
bool GlobalConfig::LoadResources() {

  LoadResource(configFileName, configName, qaConfig);

  if (cutsConfigName.empty()) {
    cuts::DataTreeCutsConfig *cutsConfigRaw{nullptr};
    cutsConfigRaw = qaConfig->GetCutsConfig();

    if (cutsConfigRaw) {
      cutsConfig = std::shared_ptr<cuts::DataTreeCutsConfig>(cutsConfigRaw);
    } else {
      DieWith("no cuts config");
      return false;
    }
  } else {
    LoadResource(configFileName, cutsConfigName, cutsConfig);
  }

  LoadResource(centralityFileName, "centr_getter_1D", centralityGetter);
  LoadResource(pidFileName, "pid_getter", pidGetter);


  return true;
}
