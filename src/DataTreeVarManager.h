//
// Created by eugene on 1/18/19.
//

#ifndef DATATREEFLOW_DATATREEVARMANAGER_H
#define DATATREEFLOW_DATATREEVARMANAGER_H

#include <DataTreeEvent.h>
#include <qa/DataTreeQAConfig.h>
#include <centrality/Getter.h>
#include <pid/Getter.h>
#include <CorrectionManager.h>
#include <cuts/DataTreeCuts.h>
#include "GlobalConfig.h"

#define gVarManager VAR::GetInstance()

#define VAR DataTreeVarManager

class DataTreeVarManager {

 public:
  static DataTreeVarManager *GetInstance() {
    static DataTreeVarManager *INSTANCE = nullptr;

    if (INSTANCE == nullptr) {
      INSTANCE = new DataTreeVarManager;
    }

    return INSTANCE;
  }

 private:
  DataTreeVarManager() = default;

 public:
  DataTreeVarManager(const DataTreeVarManager &) = delete;
  DataTreeVarManager(const DataTreeVarManager &&) = delete;

  void LoadConfig(const GlobalConfig &globalConfig);

  void InitializeVarMap(int nPsdModules);
  void InitializeCorrectionManager(Qn::CorrectionManager &manager);
  int AddVariable(const std::string& name, int NChannels = 1);

  void FillEventInfo(const DataTreeEvent &evt, double *values);
  void FillTrackInfo(const DataTreeEvent &evt, const DataTreeTrack &track, double *values);
  void FillMCTrackInfo(const DataTreeEvent &event, const DataTreeMCTrack &track, double *values);
  void FillV0Info(const DataTreeEvent &evt, const DataTreeV0Candidate &track, double *values);

  double getYBeam() const {
    return yBeam;
  }
  void setYBeam(double yBeam) {
    DataTreeVarManager::yBeam = yBeam;
  }

 private:
  int varSize{0};
  std::vector<std::tuple<std::string, int, int>> varMap;

  std::shared_ptr<Centrality::Getter> centralityGetter;
  std::shared_ptr<Pid::BaseGetter> pidGetter;
  double pidPurity{-1};

  std::shared_ptr<cuts::DataTreeCuts> cuts;

  double yBeam{0};

  int kCentrality{-1};
  int kVertexX{-1};
  int kVertexY{-1};
  int kVertexZ{-1};
  int kMreco{-1};
  int kPsdEnergyTotal{-1};
  int kRunNumber{-1};
  int kEventID{-1};
  int kPsiRP{-1};
  int kPsdEnergy{-1};
  int kPsdPhi{-1};

  int kTrackType{-1};
  int kRapidity{-1};
  int kPt{-1};
  int kEta{-1};
  int kPhi{-1};
  int kPid{-1};
  int kCharge{-1};
  int kM2{-1};
  int kP{-1};
  int kWeight{-1};
  int kChi2Vtx{-1};
  int kQP{-1};
  int kdEdx{-1};
  int kMinv{-1};

  int kCentralityVariable{-1};
  int kPidVariable{-1};

  int nPsdModules{0};
};

#endif //DATATREEFLOW_DATATREEVARMANAGER_H
