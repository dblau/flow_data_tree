//
// Created by eugene on 3/15/19.
//

#include <memory>
#include <iostream>
#include <TFile.h>
#include <TError.h>
#include <DataContainer.h>
#include <THStack.h>
#include <TStyle.h>
#include <TAxis.h>
#include <TLine.h>
#include <TBox.h>
#include <TFrame.h>
#include <TLegend.h>
#include <TH1F.h>

#include <Math/ProbFuncMathCore.h>

namespace {

using namespace std;
using namespace Qn;

typedef std::tuple<std::string, std::string> Correlant;
/* <A B> */
typedef std::tuple<Q, Q> Correlation;

/* Sqrt(<A B><A C>/<B C>) */
class Resolution {
 public:
  Resolution(const string &name, const Correlation &nom1, const Correlation &nom2, const Correlation &denom)
      : name(name), nom1(nom1), nom2(nom2), denom(denom) {

    fgResolutions.push_back(*this);
  }

  std::string name{""};
  Correlation nom1;
  Correlation nom2;
  Correlation denom;

  static std::vector<Resolution> fgResolutions;
};

std::vector<Resolution> Resolution::fgResolutions{};

typedef std::string Axis;

namespace Correlations {

const Q PSD1_X{"psd1", "X"};
const Q PSD1_Y{"psd1", "Y"};

const Q PSD2_X{"psd2", "X"};
const Q PSD2_Y{"psd2", "Y"};

const Q PSD3_X{"psd3", "X"};
const Q PSD3_Y{"psd3", "Y"};

const Correlation PSD1_PSD2_XX{PSD1_X, PSD2_X};
const Correlation PSD2_PSD3_XX{PSD2_X, PSD3_X};
const Correlation PSD1_PSD3_XX{PSD1_X, PSD3_X};

const Resolution RES_PSD1_X{"RES_PSD1_X", PSD1_PSD2_XX, PSD1_PSD3_XX, PSD2_PSD3_XX};
const Resolution RES_PSD2_X{"RES_PSD2_X", PSD1_PSD2_XX, PSD2_PSD3_XX, PSD1_PSD3_XX};
const Resolution RES_PSD3_X{"RES_PSD3_X", PSD1_PSD3_XX, PSD2_PSD3_XX, PSD1_PSD2_XX};

const Correlation PSD1_PSD2_YY{PSD1_Y, PSD2_Y};
const Correlation PSD2_PSD3_YY{PSD2_Y, PSD3_Y};
const Correlation PSD1_PSD3_YY{PSD1_Y, PSD3_Y};

const Resolution RES_PSD1_Y{"RES_PSD1_Y", PSD1_PSD2_YY, PSD1_PSD3_YY, PSD2_PSD3_YY};
const Resolution RES_PSD2_Y{"RES_PSD2_Y", PSD1_PSD2_YY, PSD2_PSD3_YY, PSD1_PSD3_YY};
const Resolution RES_PSD3_Y{"RES_PSD3_Y", PSD1_PSD3_YY, PSD2_PSD3_YY, PSD1_PSD2_YY};

}

namespace Axes {

const Axis CENTRALITY{"Centrality"};

}

DataContainerStats *GetCorrelation(TFile *ff, const Q &c1, const Q &c2) {
  std::string
      correlation_name{Form("%s_%s_%s%s", get<0>(c1).data(), get<0>(c2).data(), get<1>(c1).data(), get<1>(c2).data())};
  auto obj = dynamic_cast<DataContainerStats *>(ff->Get(correlation_name.data()));
  return obj;
}

DataContainerStats *GetCorrelation(TFile *ff, const Correlation &c) {
  auto obj = GetCorrelation(ff, get<0>(c), get<1>(c));
  if (!obj) obj = GetCorrelation(ff, get<1>(c), get<0>(c));
  return obj;
}

DataContainerStats *GetResolution(TFile *ff, const Resolution &r) {
  auto nom1 = GetCorrelation(ff, r.nom1);
  auto nom2 = GetCorrelation(ff, r.nom2);
  auto denom = GetCorrelation(ff, r.denom);
  if (!(nom1 && nom2 && denom)) {
    Error(__func__, "One of %s components is nullptr", r.name.data());
    return nullptr;
  }

  auto result = new DataContainerStats;
  *result = Sqrt((*nom1) * (*nom2) / (*denom));
  return result;
}

TH1F ChauvenetCut(const TH1F in, const std::string &name = "_chauvenet") {
  using ROOT::Math::normal_cdf;
  using ROOT::Math::normal_cdf_c;

  string new_name{in.GetName()};
  new_name.append(name);

  TH1F proto(in);
  proto.SetName(new_name.c_str());

  while (true) {
    auto mean = proto.GetMean();
    auto sigma = proto.GetStdDev();
    auto N = proto.Integral();

    const auto i_left = proto.FindFirstBinAbove(0.);
    const auto i_right = proto.FindLastBinAbove(0.);

    auto d_left = TMath::Abs(proto.GetBinCenter(i_left) - mean);
    auto d_right = TMath::Abs(proto.GetBinCenter(i_right) - mean);

    if (d_left > d_right) {
      // start from the left
      auto s = (proto.GetXaxis()->GetBinUpEdge(i_left) - mean) / sigma;
      auto prob = normal_cdf(s);
      auto n_expected = N * prob;
      auto n_actual = proto.GetBinContent(i_left);
      if (n_actual > n_expected) {
        proto.SetBinContent(i_left, 0.);
        proto.SetBinError(i_left, 0.);
        continue;
      }
    } else {
      // start from the right
      auto s = (proto.GetXaxis()->GetBinLowEdge(i_right) - mean) / sigma;
      auto prob = normal_cdf_c(s);
      auto n_expected = N * prob;
      auto n_actual = proto.GetBinContent(i_right);
      if (n_actual > n_expected) {
        proto.SetBinContent(i_right, 0.);
        proto.SetBinError(i_right, 0.);
        continue;
      }
    }

    break;
  }

  return proto;
}

}

int main(int argc, char **argv) {
  using namespace Correlations;
  using namespace Axes;

  if (argc < 2) {
    return 1;
  }

  auto input_file = shared_ptr<TFile>(TFile::Open(argv[1], "read"));

  string output_dir = argc < 3? "." : string(argv[2]);

  if (input_file) {
    Info(__func__, "File %s opened", input_file->GetName());
  } else {
    return 1;
  }

  gStyle->SetOptStat(111111);

  for (const auto &R : Resolution::fgResolutions) {
    Info(__func__, "%s", R.name.data());
    TCanvas c(R.name.data());
    string pdf_name{output_dir + "/" + R.name + ".pdf"};
    c.Print((pdf_name + "(").c_str(), "pdf");

    auto r = *GetResolution(input_file.get(), R);
    r.SetSetting(Stats::CORRELATEDERRORS);
    auto rGraphCorrelated = DataContainerHelper::ToTGraph(r);
    r.ResetSetting(Stats::CORRELATEDERRORS);
    r.SetSetting(Stats::ASYMMERRORS);
    auto rGraphAsymmErrors = DataContainerHelper::ToTGraphShifted(r, 1, static_cast<int>(r.size()) / 2);

    rGraphCorrelated->SetTitle(R.name.c_str());
    rGraphCorrelated->GetXaxis()->SetTitle(CENTRALITY.data());
    rGraphCorrelated->SetLineColor(kBlue);
    rGraphAsymmErrors->GetXaxis()->SetTitle(CENTRALITY.data());
    rGraphAsymmErrors->SetLineColor(kGreen + 2);

    rGraphCorrelated->Draw("AP");
    rGraphAsymmErrors->Draw("P,same");

    TLegend legend;
    legend.AddEntry(rGraphAsymmErrors, "ASYMMERRORS");
    legend.AddEntry(rGraphCorrelated, "CORRELATEDERRORS");
    legend.Draw();

    c.Print(pdf_name.c_str(), "pdf");
    c.Clear();

    std::vector<double> errors_fit;
    errors_fit.reserve(static_cast<unsigned long>(rGraphCorrelated->GetN()));

    TLine lineMean;
    lineMean.SetLineColor(kBlue);

    TLine propMeanLine;
    propMeanLine.SetLineColor(kRed);

    TBox bandCorrelated;
    bandCorrelated.SetFillColorAlpha(kBlue, 0.15);

    TBox bandAsymm;
    bandAsymm.SetFillColorAlpha(kGreen + 1, 0.15);


    TBox bandProp;
    bandProp.SetLineColor(kRed);
    bandProp.SetLineStyle(kDashed);
    bandProp.SetFillStyle(0);

    int iStat = 0;
    for (auto stats : r) {
      TH1F sampleMean(stats.SampleMeanHisto(R.name + "_point" + to_string(iStat)));
      TH1F sampleMeanNom1(GetCorrelation(input_file.get(), R.nom1)->At(iStat).SampleMeanHisto(
          R.name + "_point" + to_string(iStat) + "_nom1"));
      TH1F sampleMeanNom2(GetCorrelation(input_file.get(), R.nom2)->At(iStat).SampleMeanHisto(
          R.name + "_point" + to_string(iStat) + "_nom2"));
      TH1F sampleMeanDenom(GetCorrelation(input_file.get(), R.denom)->At(iStat).SampleMeanHisto(
          R.name + "_point" + to_string(iStat) + "_denom"));

      auto sampleMeanNom1Chauvenet = ChauvenetCut(sampleMeanNom1, "_chauvenet");
      sampleMeanNom1Chauvenet.SetLineColor(kRed);
      auto sampleMeanNom2Chauvenet = ChauvenetCut(sampleMeanNom2, "_chauvenet");
      sampleMeanNom2Chauvenet.SetLineColor(kRed);
      auto sampleMeanDenomChauvenet = ChauvenetCut(sampleMeanDenom, "_chauvenet");
      sampleMeanDenomChauvenet.SetLineColor(kRed);

      c.Divide(2, 1);
      c.cd(1);

      auto padLeft = gPad;
      padLeft->Divide(1, 2);
      padLeft->cd(1);

      sampleMean.Draw();
      gPad->Update();

      auto graphY = rGraphCorrelated->GetY()[iStat];
      auto graphYELoCorrelated = rGraphCorrelated->GetErrorYlow(iStat);
      auto graphYEHiCorrelated = rGraphCorrelated->GetErrorYhigh(iStat);
      bandCorrelated.DrawBox(graphY - graphYELoCorrelated, 0, graphY + graphYEHiCorrelated, gPad->GetFrame()->GetY2());

      auto graphYELoAsymm = rGraphAsymmErrors->GetErrorYlow(iStat);
      auto graphYEHiAsymm = rGraphAsymmErrors->GetErrorYhigh(iStat);
      bandAsymm.DrawBox(graphY - graphYELoAsymm, 0, graphY + graphYEHiAsymm, gPad->GetFrame()->GetY2());

      lineMean.DrawLine(graphY, 0, graphY, gPad->GetFrame()->GetY2());

      auto sigmaNom1 = sampleMeanNom1Chauvenet.GetStdDev();
      auto meanNom1 = sampleMeanNom1Chauvenet.GetMean();
      auto sigmaNom2 = sampleMeanNom2Chauvenet.GetStdDev();
      auto meanNom2 = sampleMeanNom2Chauvenet.GetMean();
      auto sigmaDenom = sampleMeanDenomChauvenet.GetStdDev();
      auto meanDenom = sampleMeanDenomChauvenet.GetMean();

      auto eps = TMath::Sqrt(
          TMath::Sq(sigmaNom1 / meanNom1) + TMath::Sq(sigmaNom2 / meanNom2) + TMath::Sq(sigmaDenom / meanDenom));
      auto propMean = TMath::Sqrt(meanNom1 * meanNom2 / meanDenom);
      auto propError = TMath::Abs(graphY / 2) * eps;
      bandProp.DrawBox(propMean - propError, 0, propMean + propError, gPad->GetFrame()->GetY2());
      propMeanLine.DrawLine(propMean, 0, propMean, gPad->GetFrame()->GetY2());

      padLeft->cd(2);
      TLegend legend;
      legend.AddEntry(&sampleMean, "sample mean histogram", "lf");
      legend.AddEntry(&lineMean, "Mean (graph)", "l");
      legend.AddEntry(&propMeanLine, "Mean (propagation)", "l");
      legend.AddEntry(&bandCorrelated, "Error (CORRELATEDERRORS)", "f");
      legend.AddEntry(&bandAsymm, "Error (ASYMMERRORS)", "f");
      legend.AddEntry(&bandProp, "Error (propagation)", "f");
      legend.DrawBox(0.1, 0.1, 0.9, 0.9);
      legend.SetBorderSize(0);
      legend.Draw();

      c.cd(2);
      auto padRight = gPad;
      padRight->Divide(1, 3);
      padRight->cd(1);
      sampleMeanNom1.Draw();
      sampleMeanNom1Chauvenet.Draw("same");
      padRight->cd(2);
      sampleMeanNom2.Draw();
      sampleMeanNom2Chauvenet.Draw("same");
      padRight->cd(3);
      sampleMeanDenom.Draw();
      sampleMeanDenomChauvenet.Draw("same");

      c.Print(pdf_name.c_str(), "pdf");
      c.Clear();
      ++iStat;
    }



    c.Print((pdf_name + ")").c_str(), "pdf");
  }

  return 0;
}

