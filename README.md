# DataTreeFlow

## Building on kronos.hpc

The flow framework exploits several `std14` features which are not
implemented yet in the standard gcc4.9 installed by default on
lxi*/lxbk* machines

To build DataTree flow in GSI farm, firstly take compiler with std14 support

```
$ module use /cvmfs/it.gsi.de/modulefiles
$ module load compiler/gcc/6
$ export CC=$(which gcc)
$ export CXX=$(which g++)
```
any gcc >= 5.1 should work, but 6.4 was tested.

ROOT has to be compiled with `-Dcxx14=ON` option as well.

Runners of the Correction/Correlations tasks are using
`Boost::program_options` to parse configuration ini file. Unfortunately,
boost provided by GSI IT is too old and DataTreeFlow fails during
linking. You can find actual boost installation in my (EK) lustre
software

```
/lustre/nyx/cbm/users/kashirin/soft/boost_1_69_0_gcc6
```

## flow.ini

```
nPsdModules       =     45
psdLayoutName     =     na61_44

[cuts]
# Path to QAConfigurations.root file
configFile        =     ../QAConfigurations.root
# Name of QA configuration in QAConfigurations.root
configName        =     pbpb_30agev_data_config
# Name of cuts configuration in QAConfigurations.root (leave empty to use default)
cutsConfig        =     pbpb_30agev_data_16_011_config_cuts

[centrality]
# Path to the output of centrality framework
centralityFile     =    centrality_pbpb30_QM.root
# Variable for the centrality estimation (@see DataTreeVarManager.h)
centralityVariable  =   PsdEnergyTotal

[sampling]
# Configuration of sampling: supported modes
# NONE - no sampling
# BOOTSTRAP
# SUBSAMPLING
samplingMode        =   BOOTSTRAP
nSamples            =   50

# Optional settings for the corrections steps of the psd subevents
# 1 - Recentering (default)
# 2 - Recentering + Twist
# 3 - Recentering + Twist + Rescale
[psd1]
correctionSteps     =       1

[psd2]
correctionSteps     =       1

[psd3]
correctionSteps     =       1
```