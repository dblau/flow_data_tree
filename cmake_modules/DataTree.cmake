#
# DataTree
#
set(DATATREE_GIT "https://gitlab.cern.ch/na61-hic/DataTree.git" CACHE STRING "Path to DataTree GIT")

set(DataTree_INSTALL_DIR ${EXTERNAL_INSTALL_DIR})
set(DataTree_INCLUDE_DIR ${DataTree_INSTALL_DIR}/include)
set(DataTree_LIBRARY_DIR ${DataTree_INSTALL_DIR}/lib)

ExternalProject_Add(DataTree_Ext
        GIT_REPOSITORY  ${DATATREE_GIT}
        GIT_TAG         "master"
        UPDATE_DISCONNECTED ${UPDATE_DISCONNECTED}
        SOURCE_DIR      "${EXTERNAL_DIR}/DataTree_src"
        BINARY_DIR      "${EXTERNAL_DIR}/DataTree_build"
        INSTALL_DIR     "${DataTree_INSTALL_DIR}"
#        CONFIGURE_COMMAND ""
#        BUILD_COMMAND     ""
#        INSTALL_COMMAND   ""
#        TEST_COMMAND      ""
        CMAKE_ARGS
            "-DEXPERIMENT=${EXPERIMENT}"
            "-DCMAKE_INSTALL_PREFIX=${DataTree_INSTALL_DIR}"
            "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
)


list(APPEND PROJECT_DEPENDENCIES DataTree_Ext)
list(APPEND PROJECT_LINK_DIRECTORIES ${DataTree_LIBRARY_DIR})
list(APPEND PROJECT_INCLUDE_DIRECTORIES ${DataTree_INCLUDE_DIR})
