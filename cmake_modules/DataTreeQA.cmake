#
# DataTreeQA
#
set(DATATREEQA_GIT "https://gitlab.cern.ch/na61-hic/DataTreeQA.git" CACHE STRING "Path to DataTreeQA GIT")

set(DataTreeQA_INSTALL_DIR ${EXTERNAL_INSTALL_DIR})
set(DataTreeQA_INCLUDE_DIR ${DataTreeQA_INSTALL_DIR}/include)
set(DataTreeQA_LIBRARY_DIR ${DataTreeQA_INSTALL_DIR}/lib)

ExternalProject_Add(DataTreeQA_Ext
        DEPENDS         "DataTree_Ext"
        GIT_REPOSITORY  ${DATATREEQA_GIT}
        GIT_TAG         "master"
        UPDATE_DISCONNECTED ${UPDATE_DISCONNECTED}
        SOURCE_DIR      "${EXTERNAL_DIR}/DataTreeQA_src"
        BINARY_DIR      "${EXTERNAL_DIR}/DataTreeQA_build"
        INSTALL_DIR     "${DataTreeQA_INSTALL_DIR}"
#        CONFIGURE_COMMAND "DATATREE_HOME=${DataTree_INSTALL_DIR} ${CMAKE_COMMAND}"
#        BUILD_COMMAND     ""
#        INSTALL_COMMAND   ""
#        TEST_COMMAND      ""
        CMAKE_ARGS
            "-DEXPERIMENT=${EXPERIMENT}"
            "-DCMAKE_INSTALL_PREFIX=${DataTreeQA_INSTALL_DIR}"
            "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
            "-DDATATREE_HOME=${DataTree_INSTALL_DIR}"
)


list(APPEND PROJECT_DEPENDENCIES DataTreeQA_Ext)
list(APPEND PROJECT_LINK_DIRECTORIES ${DataTreeQA_LIBRARY_DIR})
list(APPEND PROJECT_INCLUDE_DIRECTORIES ${DataTreeQA_INCLUDE_DIR})


