#
# Centrality
#
set(CENTRALITY_GIT "https://gitlab.cern.ch/na61-hic/centrality.git" CACHE STRING "Path to centrality GIT")

set(Centrality_INSTALL_DIR ${EXTERNAL_INSTALL_DIR})
set(Centrality_INCLUDE_DIR ${Centrality_INSTALL_DIR}/include)
set(Centrality_LIBRARY_DIR ${Centrality_INSTALL_DIR}/lib)

ExternalProject_Add(Centrality_Ext
        GIT_REPOSITORY  ${CENTRALITY_GIT}
        GIT_TAG         "master"
        UPDATE_DISCONNECTED ${UPDATE_DISCONNECTED}
        SOURCE_DIR      "${EXTERNAL_DIR}/Centrality_src"
        BINARY_DIR      "${EXTERNAL_DIR}/Centrality_build"
        INSTALL_DIR     "${Centrality_INSTALL_DIR}"
#        CONFIGURE_COMMAND ""
#        BUILD_COMMAND     ""
#        INSTALL_COMMAND   ""
#        TEST_COMMAND      ""
        CMAKE_ARGS
            "-DCMAKE_INSTALL_PREFIX=${Centrality_INSTALL_DIR}"
            "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
)

list(APPEND PROJECT_DEPENDENCIES Centrality_Ext)
list(APPEND PROJECT_LINK_DIRECTORIES ${Centrality_LIBRARY_DIR})
list(APPEND PROJECT_INCLUDE_DIRECTORIES ${Centrality_INCLUDE_DIR})
